from django.contrib import admin
from django.core.cache import cache
from .models import Story


class StoryAdmin(admin.ModelAdmin):
    model = Story

    def save_model(self, request, obj, form, change):
        cache.delete(str(obj.id))
        super(StoryAdmin, self).save_model(request, obj, form, change)


admin.site.register(Story, StoryAdmin)
