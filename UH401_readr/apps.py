from django.apps import AppConfig


class Uh401ReadrConfig(AppConfig):
    name = 'UH401_readr'
