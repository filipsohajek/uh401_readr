from bs4 import BeautifulSoup


def create_strong_tag(output, inn):
    tag_out = output.new_tag('strong', **{
        'data-cz': inn['cz'] if 'cz' in inn.attrs else None,
        'data-alt': inn['alt'] if 'alt' in inn.attrs else None,
        'data-expl': inn['expl'] if 'expl' in inn.attrs else None,
        'data-err': inn['err'] if 'err' in inn.attrs else None
    })
    tag_out.string = inn.string
    return tag_out


def parse_story_part(part_text):
    bs = BeautifulSoup(part_text)
    output = BeautifulSoup()
    story = bs.new_tag('p')
    if bs.find('inn').previous_sibling:
        story.append(bs.find('inn').previous_sibling)
    for inn in bs.find_all('inn'):
        story.append(create_strong_tag(output, inn))
        if inn.next_sibling:
            story.append(inn.next_sibling)
    output.append(story)
    return output


def get_story_html(story):
    output = BeautifulSoup()
    story_article = output.new_tag('article', **{
        'data-name': story.name,
        'data-note': story.note
    })
    for story_part in story.text.split('###'):
        story_article.append(parse_story_part(story_part))
    output.append(story_article)
    return output
