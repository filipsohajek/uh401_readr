/**
 * Highlight addon text
 */
function highlight_text(text) {
    return text.replace(/\*\*(.+?)\*\*( |$|.|,)/g,
        function (_match, sentence, post) {
            return "<b>" + sentence.replace(/\[(.+?)]/g, '</div class="no-highlight">$1<b>') + post + "</b>";
        });
}

/**
 * Add no-highlight regions where required.
 */
function clear_inn_text(text) {
    return text.replace(/\[(.+?)]/g, '<span class="no-highlight">$1</span>');
}

/**
 * Don't know what the hell is this supposed to do.
 */
function clear_addon_text(text) {
    return text === 'xxx' ? ' ' : text;
}
$(document).ready(function () {
    const story = {
        name: $("article").attr('data-name'),
        note: $("article").attr('data-note')
    };
    let part_i = 0;
    const parts = $("#story article p");
    let inn_ti = 0, inn_tcount = $("strong").length;
    const part_count = parts.length;
    let inn_i = 0;
    let inn_count = 0;
    let cur_part = null;

    parts.hide();

    $("#story strong").each(function () {
        $(this).html(clear_inn_text($(this).text()));
    });

    function next_part() {
        if (part_i === part_count) {
            $("a").hide();
        }
        cur_part = parts.eq(part_i);
        parts.eq(part_i - 1).hide();
        cur_part.show();
        inn_i = 0;
        inn_count = cur_part.find('strong').length;
        part_i += 1;
    }
    function update_top_bar() {
        $("#part-num").text("Part: " + parseInt(inn_ti) + " of " + parseInt(inn_tcount));
    }
    function next_inn() {
        $("#story").find("> p").hide(); // Hide the currently displayed part (or all of them, it doesn't matter)
        if (inn_ti >= inn_tcount) return; // The end. Launch the fireworks and celebrate

        if (inn_i === inn_count) { // We are at the end of the part.
            next_part();
        }

        const new_inn = cur_part.find('strong').eq(inn_i); // The new highlight
        cur_part.find('strong').eq(inn_i - 1).removeClass('active'); // Deactivate the old highlight
        new_inn.addClass('active'); // Activate the new one

        // Set the addon texts
        $("#addon-err").html(highlight_text(clear_addon_text(new_inn.attr('data-err'))));
        $("#addon-alt").html(highlight_text(clear_addon_text(new_inn.attr('data-alt'))));
        $("#addon-cz").html(highlight_text(clear_addon_text(new_inn.attr('data-cz'))));

        // Update the indexes and the top bar
        inn_i += 1;
        inn_ti += 1;
        update_top_bar();
    }
    $("a").click(next_inn); // Register the callback
});