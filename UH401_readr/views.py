from django.shortcuts import render, redirect
from django.core.cache import cache
from UH401_readr.models import Story
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from .services import get_story_html


def index(request):
    stories = Story.objects.all()
    paginator = Paginator(stories, 15)

    page_i = request.GET.get('page')
    try:
        stories_paged = paginator.page(page_i)
    except PageNotAnInteger:
        stories_paged = paginator.page(1)
    except EmptyPage:
        stories_paged = paginator.page(paginator.num_pages)

    return render(request, 'index.html', {
        'stories': stories_paged
    })


def story(request, story_id):
    story = Story.objects.filter(id=story_id).first()
    if not story:
        return redirect('/')
    cached_html = cache.get(str(story_id))
    if not cached_html:
        story_html = str(get_story_html(story))
        cache.set(str(story_id), story_html)
    else:
        story_html = cached_html
    return render(request, 'story.html', {
        'story': story,
        'story_xml': story_html
    })
