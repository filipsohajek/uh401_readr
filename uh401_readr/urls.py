import UH401_readr.views

from django.conf.urls import url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', UH401_readr.views.index, name="index"),
    url(r'^story/(?P<story_id>[0-9]+)$', UH401_readr.views.story, name="story")
]
